'use strict';


describe('Controller: _ZoneCtrl', function () {

    // load the controller's module
    beforeEach(module('dkAdminApp'));

    var MainCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope,$httpBackend) {
        scope = $rootScope.$new();
        MainCtrl = $controller('_ZoneCtrl', {
            $scope: scope
        });

    }));

    afterEach(inject(function ($rootScope){
        $rootScope.$apply();
    }));


    it('should have zones',inject(function ($httpBackend, $q, $timeout) {
        $httpBackend.expectGET("/drikkenapi/public/api/zones").respond([]);

        expect(scope.elements).toBeUndefined();

        $httpBackend.flush();

        expect(scope.elements).toBeDefined();
    }));

});

describe('Controller: _PostalcCtrl', function () {

    beforeEach(module('dkAdminApp'));

    var MainCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope,$httpBackend) {
        scope = $rootScope.$new();
        MainCtrl = $controller('_PostalcCtrl', {
            $scope: scope
        });

    }));

    afterEach(inject(function ($rootScope){
        $rootScope.$apply();
    }));


    it('should have postalcodes and zones',inject(function ($httpBackend, $q, $timeout) {
        $httpBackend.expectGET("/drikkenapi/public/api/postalcodes").respond([]);
        $httpBackend.expectGET("/drikkenapi/public/api/zones").respond([]);

        expect(scope.elements).toBeUndefined();
        expect(scope.zones).toBeUndefined();

        $httpBackend.flush();

        expect(scope.elements).toBeDefined();
        expect(scope.zones).toBeDefined();
    }));

});

describe('Controller: _OrdersCtrl', function () {

    beforeEach(module('dkAdminApp'));

    var MainCtrl,
        scope;

    beforeEach(inject(function ($controller, $rootScope,$httpBackend) {
        scope = $rootScope.$new();
        MainCtrl = $controller('_OrdersCtrl', {
            $scope: scope
        });
        $httpBackend.expectGET("/drikkenapi/public/api/orders").respond([]);
    }));

    afterEach(inject(function ($rootScope){
        $rootScope.$apply();
        expect(scope.elements).toBeDefined();
    }));


    it('should return user location',inject(function ($httpBackend, $q, $timeout) {
    }));

    it('should center the map',inject(function ($httpBackend, $q, $timeout) {
        expect(scope.map.center.latitude).toBe(null);

        var location = {latitude : 36,longitude : 35};

        scope.centerMap(location,13);
        expect(scope.map.center.latitude).toBe(36);
        expect(scope.map.center.longitude).toBe(35);
        expect(scope.map.zoom).toBe(13);
    }));

    it('should select an element',inject(function ($httpBackend, $q, $timeout) {
        expect(scope.selectedElement).toBe(undefined);

        var item = {name : 'primary', price : 18, location : [{latitude : 36,longitude : 35}]};

        scope.selectElement(item);
        expect(scope.selectedElement).toBe(item);

        scope.selectElement(item);
        expect(scope.selectedElement).not.toBe(item);
    }));

});