'use strict';

var app = angular.module('dkAdminApp');


app.directive('nameToUrl', function(){
	var urlPattern = ' ';
	return {
		require: 'ngModel',
		restrict: 'A',
		scope: { nameToUrl:'@',parseUrl: '=parseUrl', ngModel: '=ngModel' },
		link: function(scope,element,attrs,controller) {
			scope.$watch('parseUrl', function(val){
				scope.ngModel = scope.parseUrl;
				if(val){scope.ngModel = val.replace(/ /gi,'_');}
			});
		}
	};
});

app.directive('dOrder', function() {
	return {
		restrict: 'A',
		scope:{
	  		dOrder:'@'
	  	},
	  	link:function(scope,element,attrs,controller){
	  		element[0].onclick = function(){
	  			scope.$apply(function(){
	  				scope.$parent.options.order = scope.dOrder;
	  				scope.$parent.options.orderM === false
	  				? scope.$parent.options.orderM = true
	  				: scope.$parent.options.orderM = false;
	  			});
	  			scope.$digest();
	  		};
	  	}
	};
});


app.directive('notifications', function(ApiFactory) {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/notifications.html',
		link:function(scope,element,attrs,controller){
	  		var get =  function () {
	  				new ApiFactory.get({action:'notifications'},function success(data){
	  				scope.numNotf = data.length;
	  				scope.data = data;
	  			});
	  		};get();
	  		scope.remove = function(id){
	  			var item = new ApiFactory.delete({action:'notifications',value:id},function success(data){
	  				get();
	  			})
	  		}
	  	}
	};
});
app.directive('sidemenu', function() {
	return {
		restrict: 'E',
		require: '^ngActive',
		 scope:{
	  		ngActive:'@'
	  	},
		templateUrl : 'views/directives/side_menu.html'
	};
});

/// PRODUCTS
app.directive('addproduct', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/products/add_product.html'
	};
});

app.directive('listproducts', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/products/list_products.html'
	};
});
/// CATEGORIES
app.directive('addcategory', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/categories/add_category.html'
	};
});

///ZONES
app.directive('addzone', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/zones/add_zone.html'
	};
});
///API
app.directive('listontable', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/api/list_table.html'
	};
});
///DASHBOARD
app.directive('ordersnum', function(ApiFactory) {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/dashboard/orders_num.html',
	  	scope:{
	  		dOrder:'@'
	  	},
	  	link:function(scope,element,attrs,controller){
	  		var orders =  new ApiFactory.get({action:'orders',active:1},function success(data){
	  			scope.ordersNum = data.length;
	  			scope.data = data;
	  		});
	  	}
	};
});

app.directive('ordersearnings', function(ApiFactory) {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/dashboard/orders_earnings.html',
	  	scope:{
	  		dOrder:'@'
	  	},
	  	link:function(scope,element,attrs,controller){
	  		var orders =  new ApiFactory.get({action:'orders',active:0},function success(data){
	  			scope.sellsNum = data.length;
	  			scope.Expense = 0;
	  			scope.Sales = 0;
	  			angular.forEach(data, function(key,val){
	  				scope.Expense= parseInt(scope.Expense) + parseInt(key.price);
	  				scope.Sales = parseInt(scope.Sales) + parseInt(key.pvp);
	  			});
	  			scope.Income = scope.Sales - scope.Expense ;
	  			scope.charType = 'bar';
	  			scope.chartValues={
					"series": ["Sales","Expense","Income"],
					"data": [{
					  	"x": scope.sellsNum+" Pedidos",
						"y":[
						  	scope.Sales,
			  				scope.Expense,
			  				scope.Income
						]}
					 ]
				};
	  			scope.config  = {
					legend: {display: false,position: 'right'}
				};
                scope.data = data;
	  		});
	  	}
	};
});

app.directive('usersnum', function(ApiFactory) {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/dashboard/users_num.html',
	  	scope:{
	  		dOrder:'@'
	  	},
	  	link:function(scope,element,attrs,controller){
	  		var orders =  new ApiFactory.get({action:'orders',active:0},function success(data){
	  			scope.usersNum = 6;
	  		});
	  	}
	};
});

app.directive('alertsnum', function(ApiFactory) {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/dashboard/alerts_num.html',
	  	scope:{
	  		dOrder:'@'
	  	},
	  	link:function(scope,element,attrs,controller){
	  		var orders =  new ApiFactory.get({action:'orders',active:0},function success(data){
	  			scope.usersNum = 8;
	  		});
	  	}
	};
});
app.directive('analyticsNum', function() {
	return {
		restrict: 'E',
		templateUrl : 'views/directives/dashboard/analytics_num.html'
	};
});
app.directive('analytics', function() {
	return {
		restrict: 'E',
	  	require: '^ngViews',
	  	scope:{
	  		ngViews:'@'
	  	},
		templateUrl : 'views/directives/analytics_views.html'
	};
});