'use strict';

angular.module('dkAdminApp')


    ///MAIN
	.controller('MainCtrl',function ($rootScope,$scope,UserFactory){
		$scope.UserFactory = UserFactory;
	})


    ///DASHBOARD
	.controller('DashCtrl', function () {
})


    ///ZONE
	.controller('_ZoneCtrl', function ($scope,$location,$injector) {

        /// INVOKE PARENT
        $injector.invoke(ApiParent, this, {
            $scope: $scope
        });
        ///->OVERRIDE

        $scope.action = {action:'zones'};
        $scope.setElements($scope.action).then(
            function(data){
                $scope.elements = data;
            }
        );
	})


    ///POSTALCODE
	.controller('_PostalcCtrl', function ($scope,$location,ApiFactory,$injector) {

        /// INVOKE PARENT
        $injector.invoke(ApiParent, this, {
            $scope: $scope
        });
        ///->OVERRIDE

        $scope.action = {action:'postalcodes'};
        $scope.setElements($scope.action).then(
            function(data){
                $scope.elements = data;
            }
        );

        $scope.setElements({action:'zones'}).then(
            function(data){
                $scope.zones = data;
            }
        );
    })


    ///CATEGORIES
	.controller('_CategoriesCtrl', function ($scope,$injector) {

        /// INVOKE PARENT
        $injector.invoke(ApiParent, this, {
            $scope: $scope
        });
        ///->OVERRIDE

        $scope.action = {action:'categories',value:'',mode:'ord'};
        $scope.setElements($scope.action).then(
            function(data){
                $scope.elements = data;
            }
        );
	})


    ///FILES
    .controller('_FilesCtrl', function ($scope,$injector) {

        $scope.flowInit = {
            target:'/drikkenapi/public/service/files',
            permanentErrors:[404, 500, 501],
            minFileSize: 0
        };

        /// INVOKE PARENT
        $injector.invoke(ApiParent, this, {
            $scope: $scope
        });
        ///->OVERRIDE

        var action = 'files';
        $scope.setElements(action).then(
            function(data){
                $scope.elements = data;
            }
        );
        console.log($scope.flowInit);
    })


	///ORDERS
	.controller('_OrdersCtrl', function ($scope,$injector,LocationFactory,$location) {

		/// INVOKE PARENT
		$injector.invoke(ApiParent, this, {
			$scope: $scope
		});
		///->OVERRIDE

		var action = {action:'orders'};
        var hashSearch = undefined;

		$scope.map = {
			center: {
				latitude: null,
				longitude: null
			},
			draggable: true,
			zoom: 1
		};
		$scope.centerMap = function (Location,zoom){
			$scope.map.center.latitude = Location.latitude;
			$scope.map.center.longitude = Location.longitude;
            $scope.map.zoom = zoom;
		};
		$scope.selectElement = function(item){
			if($scope.selectedElement === item){
				$scope.selectedElement = undefined;
				$scope.getLocation();
			}else{
				$scope.selectedElement = item;
				$scope.centerMap(item.location[0],15);
			}
		};
		$scope.getLocation = function(){
			new LocationFactory.get().then(
				function(position){
					$scope.userPosition = position;
					$scope.centerMap(position.coords,13);
				}
			);
		};
        $scope.setElements(action).then(
            function(data){
                $scope.elements = data;
                new LocationFactory.get().then(function(){
                    $scope.selectElement($scope.elements[$scope.getByHash('id')]);
                })
            }
        );
    })

    ///ANALYTICS
	.controller('PageViewsCtrl', ['$scope', function ($scope) {

}])


    ///AUTH
	.controller('AuthCtrl', function ($scope,$http,UserFactory,$location){
		$scope.login = function(){
			$http({
				method:'POST',
				url:'/drikkenapi/public/service/auth',
				data:{
					email:'vendor@vendor.com',///Sanitize here
					password:'vendor'
				}
			}).success(function(data){
				UserFactory.isLogged = true;
                UserFactory.user = data.user.email;
                UserFactory.token = data.token;

				console.log(UserFactory);
				$location.path('/products');
			}).error(function(data){
				console.log(data);
                $location.path('/');
			});
		};
		$scope.login();
	});