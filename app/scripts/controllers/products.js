'use strict';

var app = angular.module('dkAdminApp');

   ///INHERIT FROM APICTRL
app.controller('_ProductsCtrl', function ($scope,ProductService,$location,$injector,AlertFactory,$modal) {

	$scope.action = 'products';

    /// INVOKE PARENT
    $injector.invoke(ApiParent, this, {
        $scope: $scope
    });
    ///->OVERRIDE

	/// SET SCOPES
	$scope.setElements = function(){
	    new ProductService.get({action:'products'}).then(
	        function success(data){
	            $scope.elements =  data;
                AlertFactory.ok('Productos cargados');
	        },
	        function error(data){
	            new AlertFactory.alert(data);
	        }
	    );
	};
	/// ACTIONS
	$scope.newElement = function(form){
	    new ProductService.post(form,{action:'products'}).then(
	        function success(data){
                $scope.pForm.flow.resume();
                $scope.setElements();
                AlertFactory.ok('Producto añadido');
	        }
        );
	};

	$scope.editElement = function(form){
	    new ProductService.post(form,{action:'products',value:'edit'}).then(
	        function success(data){
	            $scope.setElements();
	        },
	        function error(data){
	            new AlertFactory.alert(data);
	        }
	    );
	};

    $scope.removeItem = function (item){
        $scope.deleteElement(item,'products').then(function (data){
            $scope.setElements();
        })
    };

    $scope.removeImage = function(item,key){
        $scope.deleteElement(item.image,'files').then(function(){
            $scope.deleteElement(item.image,'productsimages').then(function (data){
                $scope.setElements();
                $scope.pForm.images.splice(key,1);
            })
        });
    }

	//NEW
	$scope.categories = {};
	$scope.tags = {};

	$scope.hash = $location.hash();

	$scope.options = {
	    order:'name',
	    orderM:false,
	    filter: $scope.hash
	};

	///ACTIONS
	var setCategories = function (){
	    new ProductService.get({action:'categories'}).then(
	        function success(data){
	            $scope.categories = data;
	        }
	    );
	};

	var setTags = function(){
	    new ProductService.get({action:'tags'}).then(
	        function success(data){
	            $scope.tags = data;
	        }
	    );
	};

    var setFiles = function(){
        new ProductService.get({action:'files'}).then(
            function success(data){
                $scope.images = data;
            }
        );
    };

	///PLUGINS
	$scope.selectize = {};

	$scope.selectize.categories = {
	    options: {
	        valueField: 'id',
	        labelField: 'name',
	        searchField: ['name'],
	        create: false
	    }
	};

	$scope.selectize.tags = {
	    options: {
	        valueField: 'id',
	        labelField: 'name',
	        searchField: ['name'],
	        create: false
	    }
	};

	$scope.tinymceOptions = {
	    menubar : false,
	    statusbar: false,
	    plugins: 'code',
	    toolbar: 'styleselect | bold italic code  | bullist outdent indent'
	}

    $scope.flowInit = {
        target:'/drikkenapi/public/service/files',
        permanentErrors:[404, 500, 501],
        minFileSize: 0
    };

    // MODALS
    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: '_FilesCtrl',
            resolve: {
                items: function () {
                    return $scope.assoc;
                }
            }
        });
        modalInstance.result.then(function (selectedItem) {
        }, function () {
            console.log("dismissed");
        });
    };

	///RUN
	$scope.setElements();
	setTags();
	setCategories();

    $scope.restartForm = function(){
        $scope.pForm.flow.cancel();
        angular.forEach($scope.pForm,function(value,index){
            if(angular.isString(value) || angular.isArray(value)|| angular.isNumber(value)){
                $scope.pForm[index] = null;
            }
        });
        $scope.pForm.$setPristine();
        $scope.editToggle = false;
        $scope.selectElement($scope.selectedElement);
    }
    $scope.restartView = function(){
        $location.hash('');
        $scope.options.filter = null;
        $scope.selectElement($scope.selectedElement);
        $scope.restartForm();
    }
    $scope.seeForm = function(){
        console.log($scope.pForm);
    }
});
