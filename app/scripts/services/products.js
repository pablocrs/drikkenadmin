'use strict';

app = angular.module('dkAdminApp');

app.service('ProductService', ['ApiFactory','$q','UserFactory',function(ApiFactory,$q,UserFactory){

	this.get = function(action){
		var deferred = $q.defer();

		new ApiFactory.get(action,
			function success(data){
				deferred.resolve(data);
			},
			function error(data){
				deferred.reject(data);
			}
		);
		var promise = deferred.promise;
		return $q.all(promise);
	};

	this.post = function(form,params){
		var deferred = $q.defer();
		var api = new ApiFactory();
		angular.forEach(form, function(value,key){
			if(angular.isString(value)){
				api[key] = value;
			}
		});
        api['_token']= UserFactory.token;

		api.$save(params,
			function success(data){
				var categories = form.categories;
				var tags = form.tags;
                var images = {};
                angular.forEach(form.flow.files,function(value,key){
                    images[key] = value.relativePath;
                });

				postLoop(categories,data['id'],'category',{action:'productscategories'});
				postLoop(tags,data['id'],'tag',{action:'productstags'});
				postLoop(images,data['id'],'image',{action:'productsimages'});

				deferred.resolve(data);
			},
			function error(data){
				deferred.reject(data);
			}
		);
		var promise = deferred.promise;
		return $q.all(promise);
	};

	var postLoop = function(data,item,type,params){
        var deferred = $q.defer();
        if(typeof(data) === 'string'){
            data = data.split(",");
		}

        if(typeof(data) === 'object'){
            angular.forEach(data,function(value){
                var api = new ApiFactory();

                api.product = item;
                api[type] = value;

                api.$save(params);
            });
            deferred.resolve(data);
        }
        else{
            deferred.reject(data);
        }

        var promise = deferred.promise;
        return $q.all(promise);
	};
    this.postLoop = postLoop;

	this.delete = function(item,action){
		var deferred = $q.defer();
        var api = new ApiFactory();

		api.$delete({action:action,value:item,_token:UserFactory.token},
			function success(data){
				deferred.resolve(data);
			},
			function error(data){
				deferred.reject(data);
			}
		);

        var promise = deferred.promise;
		return $q.all(promise);
	};

}]);
