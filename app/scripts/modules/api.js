'use strict';

var apiModule = angular.module('ApiModule',[]);

apiModule.factory('ApiFactory', ['$resource','UserFactory',function($resource,UserFactory) {
    return $resource('/drikkenapi/public/api/:action/:value',false,
    {
        'get' : {method:'GET',params: {action: '@action',value:UserFactory.user},isArray:true,headers:{ 'Content-type': 'text/json' }},
        'post' : {method:'POST',params: {action: '@action',value:'@value'},isArray:false,headers:{ 'Content-type': 'text/json' }},
        'delete' : {method:'DELETE',params: {action: '@action',value:'@value'},isArray:false,headers:{ 'Content-type': 'text/json' }}
    })
}]);

///API CLASS-
function ApiParent($scope,ApiFactory,AlertFactory,UserFactory,$q,$location){

    this.action = '';

    /// ACTIONS
    $scope.setElements = function(action){
        action === undefined ? action = $scope.action : action;
        var deferred = $q.defer();

        new ApiFactory.get(action,
            function success(data){
                AlertFactory.ok(data);
                deferred.resolve(data);
            },
            function error(data){
                AlertFactory.alert(data);
                deferred.reject(data);
            }
        );

        var promise = deferred.promise;
        return $q.all(promise);
    };
    $scope.newElement = function(form){
        var deferred = $q.defer();
        var element = new ApiFactory();

        element.action = $scope.action;
        angular.forEach(form,function(value,key){
            if(angular.isString(value))element[key] = value;
        });

        element['_token']= UserFactory.token;

        element.$post(
            function success(data){
                AlertFactory.ok(data);
                deferred.resolve(data);
            },
            function error(data){
                AlertFactory.alert(data);
                deferred.reject(data);
            }
        );

        var promise = deferred.promise;
        return $q.all(promise);
    };

    $scope.deleteElement = function(item,action){
        var deferred = $q.defer();
        var api = new ApiFactory();
        console.log(api);
        api.$delete({action:action,value:item,_token:UserFactory.token},
            function success(data){
                deferred.resolve(data);
            },
            function error(data){
                AlertFactory.alert(data);
                deferred.reject(data);
            }
        );

        var promise = deferred.promise;
        return $q.all(promise);
    };

    /// HELPERS
    $scope.editMode = function(item){
        $scope.editToggle = true;
        if($scope.selectedElement != item){
            angular.forEach(item, function(value,key){
                if(angular.isArray(value)){
                    var array = [];
                    angular.forEach(value, function(val,k){
                        array.push(val.id);
                    });
                    $scope.pForm[key] = array.join(",");
                    console.log(key);
                    console.log(value);
                }
                else{
                    $scope.pForm[key] = value;
                }
                if(key === 'images'){
                    $scope.pForm[key] = value;
                }
            });
            $scope.pForm.$pristine = false;
            $scope.pForm.$dirty = true;
            $scope.selectElement(item);
        }
        else{
            $scope.restartForm();
        }
    };

    $scope.selectElement = function(item){
        if($scope.selectedElement == item){
	        $scope.selectedElement = undefined;
        }else{
	        $scope.selectedElement = item;
           // item.url != undefined ? $location.hash(item.url) : $location.hash(item.id);
        }
    };

    $scope.cleanForm = function(form){
        form.$setPristine();
    };

    $scope.isUnchanged = function(user) {
        return angular.equals(user, $scope.master);
    };

	$scope.options = {
		order:'name',
		orderM:false,
		filter: undefined
	};
    /// RETURN ARRAY KEY BY HASH
    $scope.getByHash = function(mode){
        var item = undefined;
        if($location.hash() != undefined){
            angular.forEach($scope.elements,function(v,k){
                if(v[mode] == $location.hash()){
                    console.log(k);
                    item = k;
                }
            });
            return item;
        }
    };
}
