'use strict';

var app = angular.module('dkAdminApp', [
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'ApiModule',
	'ngRoute',
	'ngAnimate',
	'MessageCenter',
	'textAngular',
	'flow',
	'angularCharts',
	'selectize-ng',
	'google-maps',
    'ui.bootstrap'
])
	.config(function ($routeProvider, $provide, $locationProvider, flowFactoryProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/auth.html',
				controller: 'AuthCtrl'
			})
			.when('/home', {
				templateUrl: 'views/dashboard.html',
				controller: 'DashCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/products', {
				templateUrl: 'views/products.html',
				controller: '_ProductsCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/categories', {
				templateUrl: 'views/categories.html',
				controller: '_CategoriesCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/zones', {
				templateUrl: 'views/zones.html',
				controller: '_ZoneCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/postalcodes', {
				templateUrl: 'views/zones.html',
				controller: '_PostalcCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/files', {
				templateUrl: 'views/files.html',
				controller: '_FilesCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.when('/orders', {
				templateUrl: 'views/orders.html',
				controller: '_OrdersCtrl',
				resolve: {
					logged: function (CheckService) {
						return  CheckService.check();
					}
				}
			})
			.otherwise({
				redirectTo: '/'
			});

		flowFactoryProvider.defaults = {
			target: 'http://192.168.1.128/drikkenapi/public/service/files',
			permanentErrors: [404, 500, 501],
			maxChunkRetries: 1,
			chunkRetryInterval: 5000,
			simultaneousUploads: 4
		};
		flowFactoryProvider.on('catchAll', function (event) {
			console.log('catchAll', event);
		});

	});


app.service('CheckService', ['$rootScope','UserFactory','$location','$q',function ($rootScope,UserFactory,$location,$q) {
	this.check = function(){
		var deferred = $q.defer();
		if(!UserFactory.isLogged){
			deferred.reject($location.path('/'));
		}else{
			/// REMINDER
			deferred.resolve(UserFactory);
		}
		var promise = deferred.promise;
		return $q.all(promise);
	};
}]);

app.factory('UserFactory', [function () {
	return {
		isLogged: false,
		user: '',
		token:'',
		userinfo:{}
	};
}]);


app.factory('ApiFactory', ['$resource','UserFactory',function($resource,UserFactory) {
	return $resource('/drikkenapi/public/api/:action/:value',false,
	{
		'get' : {method:'GET',params: {action: '@action',value:UserFactory.user},isArray:true,headers:{ 'Content-type': 'text/json' }},
		'post' : {method:'POST',params: {action: '@action',value:'@value'},isArray:false,headers:{ 'Content-type': 'text/json' }},
		'delete' : {method:'DELETE',params: {action: '@action',value:'@value'},isArray:false,headers:{ 'Content-type': 'text/json' }}
	})
}]);


app.factory('AlertFactory', ['MessageService','$log',function (MessageService,$log) {
	return {
		alert : function(msj){
			angular.forEach(msj.data,function(value){
				$log.error(value);
				if(angular.isArray(value)){
					MessageService.broadcast(value[0],{color: 'warning', important:true,timeout:3000});
				}
				else{
					MessageService.broadcast(msj.status,{color: 'warning', important:true,timeout:3000});
				}
			});
		},
		ok : function(msj){
			$log.info(msj);
			MessageService.broadcast(msj,{color: 'success', important:true,timeout:1500});
		},
		info : function(msj){
			$log.info(msj);
			MessageService.broadcast(msj,{color: 'primary', important:true,timeout:3000});
		}
	};
}]);


app.service('LocationFactory', ['$q','$window',function($q,$window){
	this.get = function(){
		var deferred = $q.defer();

		$window.navigator.geolocation.getCurrentPosition(
		function(position){
			deferred.resolve(position);
		},
		function(error){
			deferred.reject(error);
		});

		var promise = deferred.promise;
		return $q.all(promise);
	};
}]);


app.module("template/message-center/message-center.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("template/message-center/message-center.html",
        '<span>' +
        '<span class="message-center-important">' +
        '<message ng-repeat="message in impMessages" class="message-animation"></message>' +
        '</span>' +
        '<span class="message-center-regular">' +
        '<message ng-repeat="message in messages" class="message-animation"></message>' +
        '</span>' +
        '<span>');
}]);

app.module("template/message-center/message.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("template/message-center/message.html",
        '<div class="message-box" ng-class="message.classes">' +
        '<span class="message">{{message.message}}</span>' +
        '<button type="button" class="tiny success close" aria-hidden="true" ng-click="removeItem(message)">&times;</button>' +
        '</div>');
}]);